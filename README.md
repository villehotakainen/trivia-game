
## Trivia Game

Trivia-Game is a single page Vue web-application that gets trivia questions from an API and saves the users high-scores on a JSON server. Uses the Open Trivia Api for questions, and http://noroffapi.herokuapp.com/trivia for storage of user data. Noroffapi is based on https://github.com/dewald-els/noroff-assignment-api

## Installation

Clone project, install the dependencies and start the server.

```sh
git clone https://gitlab.com/villehotakainen/trivia-game.git
cd trivia-game
npm i
npm run serve
```
Or use it in heroku: https://triviagamevue.herokuapp.com/#/

## License

MIT
