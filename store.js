import Vue from 'vue'
import Vuex from 'vuex'
import { getUsername } from './src/api/noroffApi'
import { addUser, shouldUserHighScoreUpdate } from './src/api/noroffApi'
import { fetchCategories, fetchQuestions } from './src/api/triviaApi'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        userData: {
            username: '',
            highScore: 0,
        },
        categories: [],
        questions: [],
        categoriesError: null,
        questionsError: null,
        quizOptions: {
            difficulty: null,
            amount: null,
            category: null,
        }
    },
    getters: {
        // returns state categories
        returnCategories: state => {
            return state.categories
        },
        // returns highscore state
        returnScore: state => {
            return state.userData.highScore
        },
        // returns category state
        selectedQuizCategory: state => {
            return state.quizOptions.category
        },
        // returns question state
        returnQuestions: state => {
            return state.questions
        },
        // returns question from questions array that matches to parameter id
        returnCurrentQuestionWithID: (state) => (id) => {
            return state.questions.find(q => q.id === id)
        }
    },
    mutations: {
        // sets userdata state
        setUserData: (state, payload) => {
            state.userData = payload
        },
        // maps index to questions and collects all answers to question state
        setQuestions: (state, payload) => {
            state.questions = payload.map((q, index) => {
                const all_answers = [...q.incorrect_answers, q.correct_answer].sort()
                return {
                    id: index,
                    all_answers,
                    user_answer: null,
                    ...q
                }
            })
        },
        // sets user's highScore state
        setUserHighScore: (state, payload) => {
            state.userData.highScore += payload.highScore
        },
        // sets user's highScore state to 0
        setUserScoreToZero: (state) => {
            state.userData.highScore = 0
        },
        // sets state categories
        setCategories: (state, payload) => {
            state.categories = payload
        },
        //sets state category error
        setCategoriesError: (state, payload) => {
            state.categoriesError = payload
        },
        // sets quiz options state
        setQuizOptions: (state, payload) => {
            state.quizOptions = payload
        },
        // sets state question error
        setQuestionsError: (state, payload) => {
            state.questionsError = payload
        },
        // sets incoming answer to question state user by id
        setQuestionUserAnswer: (state, payload) => {
            state.questions = state.questions.map((q) => q.id === payload.id ? { ...q, user_answer: payload.answer } : q)
        }
    },
    actions: {
        // Function that returns the response of category API fetch result to category state
        async fetchTriviaCategories({ commit }) {
            const [error, categories] = await fetchCategories()
            commit('setCategoriesError', error)
            commit('setCategories', categories)
        },
        // Function that returns the response of questions API fetch result to question state
        async fetchTriviaQuestions({ commit, state }) {
            const [error, questions] = await fetchQuestions(state.quizOptions)
            commit('setQuestionsError', error)
            commit('setQuestions', questions)
        },
        // Function that adds object with username to DB if it doesn't exist yet
        async addUserToDb({ state }) {
            const current = await getUsername(state.userData)
            if (current.data.length === 0) {
                addUser(state.userData)
            }
        },
        // Function that handles passing state as a parameter to database update call
        updateUserHighScore({ state }) {
            shouldUserHighScoreUpdate(state.userData)
        },
    }
})