import axios from "axios"

// returns categories from opentdb api
export const fetchCategories = async () => {
    try {
        const { trivia_categories } = await (await axios.get('https://opentdb.com/api_category.php')).data
        return [null, trivia_categories]
    } catch (e) {
        return [e.message, []]
    }
}
// returns user decided questions from opentdb api
export const fetchQuestions = async (params) => {
    try {
        const { results } = await (await axios.get('https://opentdb.com/api.php', { params })).data
        return [null, results]
    } catch (e) {
        return [e.message, []]
    }
}