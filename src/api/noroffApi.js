const apiURL = "https://noroffapi.herokuapp.com/trivia";
const apiKey = "VIId4HSkvmTLYJwX";
import axios from "axios";

// returns user from database that corresponds to parameter
export const getUsername = async (params) => {
  return await axios.get(`${apiURL}?username=${params.username}`);
};

// adds user to database
export function addUser(param) {
  fetch(apiURL, {
    method: "POST",
    headers: {
      "X-API-Key": apiKey,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: param.username,
      highScore: param.highScore,
    }),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Could not create new user");
      }
      return response.json();
    })
    .catch((error) => {
      console.log(error);
    });
}

export function shouldUserHighScoreUpdate(param) {
  getUsername(param).then(
    result => {
      if (param.highScore > result.data[0].highScore) {
        fetch(`${apiURL}/${result.data[0].id}`, {
          method: 'PATCH',
          headers: {
            'X-API-Key': apiKey,
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            highScore: param.highScore
          })
        })
          .then(response => {
            if (!response.ok) {
              throw new Error('Could not update high score')
            }
            return response.json()
          })
          .catch(error => {
            console.log(error)
          })
        return result
      }
    })
}
